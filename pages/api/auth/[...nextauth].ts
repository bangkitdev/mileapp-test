import NextAuth, { NextAuthOptions } from "next-auth";
import CredentialsProvider from 'next-auth/providers/credentials'

const authOptions: NextAuthOptions = {
    session: {
        strategy: 'jwt'
    },
    providers: [
        CredentialsProvider({
            type: 'credentials',
            credentials: {},
            authorize(credentials, req) {
                const { username, password } = credentials as { username: string; password: string }

                //hardcode login check
                if (username !== 'admin' || password !== 'mileapp') {
                    throw new Error('Invalid')
                }

                //if authenticated
                return {
                    id: '@23',
                    name: 'admin'
                }
            }
        })
    ],
    pages: {
        signIn: '/login'
    }
}
export default NextAuth(authOptions)
