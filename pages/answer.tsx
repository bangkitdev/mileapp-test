import Image from 'next/image';
const Answer = () => {
    const swap = (a: string, b: string): string => {
        b = [a, a = b][0];
        return (`A = ${a} dan B = ${b}`)
    }
    const numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
    const numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

    const arrays = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."];



    const mapping = (arr: string[]): { [key: string]: string; } => {
        return arr.reduce((tree: { [key: string]: any }, path: string) => {
          let node: any = tree;
          for (let j = 0; j < path.split(".").slice(0, -1).length; j++) {
            const key = path.split(".").slice(0, -1)[j];
            if (path.split(".").length === 4 && j === 2) {
              node[key] = path.split(".").slice(0, -1).join(".");
            } else {
              node = node[key] || (node[key] = {});
            }
          }
          return tree;
        }, {});
      };

    const maptree = mapping(arrays)

    const createTree = (obj: { [key: string]: any }): JSX.Element => {
        return (
            <ul className="pl-1">
                {Object.keys(obj).map(key => (
                    <li className="pl-4" key={key}>
                        <span className="pl-4">{key}</span>
                        <span className="pl-4">{typeof obj[key] === "object" ? createTree(obj[key]) : obj[key]}</span>
                    </li>
                ))}
            </ul>
        );
    }
    const findMissingNumber = (numb: number[]) => {
        const max: number = Math.max(...numb);
        const min: number = Math.min(...numb);
        const missing = []

        for (let i = min; i <= max; i++) {
            if (!numb.includes(i)) {
                missing.push(i);
            }
        }
        return missing;
    }
    const findDuplicates = (arr: number[]) => {
        let sorting = arr.slice().sort();
        let results: number[] = [];
        for (let i = 0; i < sorting.length - 1; i++) {
            if (sorting[i + 1] === sorting[i]) {
                results.push(sorting[i]);
            }
        }
        const strip = results.filter(function (item, state) {
            return results.indexOf(item) === state;
        })

        return strip;
    }

    return (
        <div className=" leading-6 font-light container mx-auto px-3 max-w-2xl">
            <h2 className="text-4xl font-bold pb-10 pt-5">Questionnaires</h2>
            <h4 className=" text-gray-800 text-xl my-3 font-bold">Do you prefer vuejs or reactjs? Why ?</h4>

            <p>I prefer react, because I don't know much about&nbsp;vue, Goes back in the beginning when I decided to choose which framework should I pick , I choose react because I was convinced by the community, at that time react had much larger community than vue, and also by the community react has much mature ecosystem, react also backed by big company (META) at least&nbsp; it proves that react had used by Giant apps such as Instagram and Facebook,</p>

            <br />
            <p> React also guarantee that I can relatively easy to jump into mobile dev ecosystem by doing React native, and in addition react is un-opinionated js library that we as developer has liberty to choose which stack/path to solve a problem. I am not discouraging Vue it's just I pick react first and still learning doing it ever since, and I will continue my study to&nbsp;Vue, svelte or solidjs in the near future.</p>
            <br />
            <br />
            <h4 className=" text-gray-800 text-xl my-3 font-bold">What complex things have you done in frontend development ?</h4>
            <p>A long as I can recall redesign phase is the most complex situation, we had existing production site but the stake holders want it to redesign, we have to deal with complex situation such as&nbsp;SEO , design system, framework to pick and API updates. It was very frustrating moment to build frontend from the ground up without breaking the existing one. I was chosen to build css framework by that time there was no internal framework existing yet, so we have to deal with almost every stake holders to ensure the framework suit the needs of redesign phase.</p>

            <br />
            <br />

            <h4 className=" text-gray-800 text-xl my-3 font-bold">why does a UI Developer need to know and understand UX? how far do you understand it?</h4>

            <p>I matters a lot, a developer has to know UX because software development it all about UX, we serve Human not a robot. How far I know? as far as I am a human to I can say the app is good or bad by using IT, as a developer I would much apriciated if I been involved in sprint design or such, that I can discuss about the design or UX.</p>
            <br />
            <br />
            <h4 className=" text-gray-800 text-xl my-3 font-bold">Give your analysis results regarding <a className="text-sky-500" href="https://taskdev.mile.app/login">https://taskdev.mile.app/login</a> from the UI / UX side!</h4>

            <p>First thing that had me uncomfortable is the organization name, why and how the name it's required in the authentication process, doesn't it should be dependent on the user attributes? for my perspective the system should know me and my organitation when I submit my username and password only.</p>

            <p>However let say, the organization name is required for what ever reason, Then I will make it separately from username and password, I will make 2 steps, first the organization name field, then the username password form, it feels much easy to deal with 1 or 2 input fields than 3 fields together and disabled one or the other first, also ideally we can use captcha after 3 or more attemps to prevent attack from the input field. in addition I prefer "form layout" in the middle because it feels close to my hand ,to move the cursor into the center because I use right hand to move it.</p>
            <hr className="my-10" />
            <h2 className="text-4xl font-bold pb-10 pt-5">Logic</h2>

            <h4 className=" text-gray-800 text-xl my-3 font-bold">
                6.a Swap the values of variables A and B</h4>
            <div className="bg-gray-200 text-gray-600 p-10">
                    // terdapat 2 variabel A & B
                <br />
                A = 3
                <br />
                B = 5
                <br />
                <br />
                    // Tukar Nilai variabel A dan B, Syarat Tidak boleh menambah Variabel Baru
                <br />
                <br />
                    // Hasil yang diharapkan :
                <br />
                A = 5
                <br />
                B = 3
                <br />
                <br />
                <strong>Answer</strong>
                <br />
                Please see the running code on gitlab
                <br />
                <br />
                {`swap('3','5')`}
                <br />
                {swap('3', '5')}
                <br />
                <br />
                <p>
                    <Image src="/swap.svg" width="900" height="100" alt="swap" />
                </p>
                <br />

            </div>
            <h4 className=" text-gray-800 text-xl my-3 font-bold">
                6.b Find the missing numbers from 1 to 100</h4>
            <div className="bg-gray-200 text-gray-600 p-10">
                numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
                <br />
                <br />
                Answer
                <br />
                Please see the running code on gitlab
                <br />
                ({
                    findMissingNumber(numbers).join(', ')
                })
                <br />
                <br />
                <p>
                    <Image src="/findmissing.svg" width="900" height="100" alt="swap" />
                </p>
                <br />
                <br />
            </div>
            <br />
            <h4 className=" text-gray-800 text-xl my-3 font-bold">
                6.c return the number which is called more than 1</h4>
            <div className="bg-gray-200 text-gray-600 p-10">
                numbers =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
                <br />
                <br />
                Answer
                <br />
                Please see the running code on gitlab
                <br />
                ({
                    findDuplicates(numbers2).join(', ')
                })
                <br />
                <br />
                <p>
                    <Image src="/find-duplicate.svg" width="900" height="100" alt="swap" />
                </p>
                <br />
                <br />
            </div>
            <br />
            <h4 className=" text-gray-800 text-xl my-3 font-bold">
                6.d</h4>
            <div className="bg-gray-200 text-gray-600 p-10">
                I answer this using PHP, please cek the code in the README.md at gitlab
                <br />
                <br />
                <br />
                --edited
                <br />
                <br />
                <br />
                I managed to update the answer in javascript
                {
                    createTree(maptree)
                }
                <br />
                <br />
                <p>
                    <Image src="/shorter-map.svg" width="900" height="100" alt="swap" />
                </p>
                <br />
                <br />
            </div>
            <br />
        </div>
    )
}

export default Answer
