import { NextPage } from 'next';
import { signOut, useSession } from 'next-auth/react';
import Router from 'next/router';
import { useEffect, useRef } from 'react';
import { markers } from './api/markers';
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';

const Maps: NextPage = () => {

    const { status, data } = useSession()

    const mapContainer = useRef<any>(null);
    const map = useRef<mapboxgl.Map | any>(null);

    useEffect(() => {

        if (status !== 'authenticated') {
            Router.push('/login')
        } else {

            mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_GL_ACCESS_TOKEN ?? '';
            map.current = new mapboxgl.Map({
                container: mapContainer.current,
                style: "mapbox://styles/mapbox/streets-v12",
                center: [106.8302802, -6.1769083],
                zoom: 13,
            });
            
            const geoMap = {
                type: 'Feature',
                features: markers.map((marker) => ({
                    attributes: {
                        place: marker.place,
                        city: marker.city,
                        iconSize: [40, 40],
                        icon: marker.iconType,
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: {
                            lat: marker.lat,
                            lng: marker.long
                        }
                    }
                }))
            };
            map.current.on('load', () => {
                geoMap.features.forEach((marker) => {
                    const markerIcon = document.createElement('div');
                    markerIcon.className = `map-marker ${marker.attributes.icon}`;
                    markerIcon.style.width = marker.attributes.iconSize[0] + 'px';
                    markerIcon.style.height = marker.attributes.iconSize[1] + 'px';
    
                    new mapboxgl.Marker(markerIcon)
                        .setLngLat(marker.geometry.coordinates)
                        .setPopup(
                            new mapboxgl.Popup({ offset: 25 }).setHTML(
                                `<p>${marker.attributes.place}, ${marker.attributes.city}</p>`
                            )
                        )
                        .addTo(map.current);
                });
            });
        }

    }, [status]);


    if (status !== 'authenticated') {
        return <div className='flex items-center justify-center min-h-screen text-gray-300'>Loading...</div>
    }
    return (
        <>
            <div className="text-center flex items-center justify-center z-50 fixed right-2 top-2">
                <button className="px-10 py-2 rounded bg-red-400 text-white text-sm" onClick={() => signOut({ callbackUrl: '/' })}>Logout</button>
            </div>

            <div className="map-container min-w-full min-h-screen" ref={mapContainer} />
        </>

    )
}

export default Maps


