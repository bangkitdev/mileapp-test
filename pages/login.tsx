import { NextPage } from 'next';
import Image from 'next/image';
import React, { useRef, useState } from 'react';
import Input from '../components/ui/Input';
import { signIn } from 'next-auth/react';
import { useSession } from 'next-auth/react';
import Router from 'next/router';
import { useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';

const Login: NextPage = () => {
    const username = useRef<HTMLInputElement>(null)
    const password = useRef<HTMLInputElement>(null)
    const org = useRef<HTMLInputElement>(null)
    const [inputType, setInputType] = useState('password')
    const [isIconEyeOpen, setIconEyeOpen] = useState(false)
    const [isLoggingIn, setLoggingIn] = useState(false)
    const [usernameErrorMessage, setUsernameErrorMessage] = useState('')
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
    const [orgErrorMessage, setOrgErrorMessage] = useState('');
    const [isAuthenticated, setIsauthenticated] = useState(false)
    const [isSubmited, setSubmited] = useState(false)
    const [isOrgValid, setOrgValidity] = useState(false)
    const [isDropdown, setDropdown] = useState(false)
    const [langguage, setLanguage] = useState('id')

    const { t } = useTranslation('common');

    const { status, data } = useSession()
    useEffect(() => {
        if (status === 'authenticated') {
            Router.replace('/maps')
        }
    }, [status])

    const orgHanlder = () => {
        const organization: string = org.current!.value

        if (organization.trim() === '') {
            setOrgErrorMessage(t('OrgErrorMessage'))
            return
        }
        if (organization.toLowerCase() === 'mileapp') {
            setOrgValidity(true)
            setOrgErrorMessage('')
            return
        }
        setOrgErrorMessage(t('OrgIncorrectMessage'))
    }

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            orgHanlder()
        }
    }

    const onSubmitHandler = async (e: React.FormEvent<HTMLFormElement>) => {
        interface DataType {
            username: string,
            password: string
            organization: string
        }

        e.preventDefault();
        const uname: string = username.current!.value
        const pass: string = password.current!.value
        const orgz: string = org.current!.value

        //check valid
        if (uname.trim() === '' || pass.trim() === '') {
            setUsernameErrorMessage(t('UsernameErrorMessage'))
            setPasswordErrorMessage(t('PasswordErrorMessage'))
            return
        }
        setLoggingIn(true)

        const dataLogin: DataType = {
            username: uname,
            password: pass,
            organization: orgz,
        }

        const res = await signIn('credentials', {
            ...dataLogin,
            redirect: false
        })
        if (res?.error !== null) {
            setSubmited(true);
            setLoggingIn(false)
            setIsauthenticated(false)
        }

    }
    const passwordReveal = () => {
        inputType === 'password' ? setInputType('text') : setInputType('password')
    }
    const iconEyeHanlder = () => {
        setIconEyeOpen(true)
    }
    const formEmptyCheck = (name: string, val: string) => {
        if (val.trim() === '') {
            switch (name) {
                case 'uname':
                    setUsernameErrorMessage(t('UsernameErrorMessage'))
                    break
                case 'org':
                    setOrgErrorMessage(t('OrgErrorMessage'))
                    break
                default:
                    setPasswordErrorMessage(t('PasswordErrorMessage'))
            }
        } else {
            switch (name) {
                case 'uname':
                    setUsernameErrorMessage('')
                    break
                case 'org':
                    setOrgErrorMessage('')
                    break
                default:
                    setPasswordErrorMessage('')
            }

        }
    }
    const blurHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputName = e.target.name;
        const inputValue = e.target.value;
        formEmptyCheck(inputName, inputValue)
    }
    const dropdownHandler = (l = 'id') => {
        setDropdown(!isDropdown)
        setLanguage(l)
    }
    return (
        <div>
            <div className="bg md:bg-[url('/sit.png')] min-h-[90vh] relative">
                <div>
                    <div className='text-right text-xs sm:text-base bg-sky-100 py-1 px-3 text-gray-600 flex justify-end relative'>
                        {t('headTagline')} : +62 812-1133-5608
                        <button onClick={()=> dropdownHandler(langguage)} className='flex items-center ml-4 bg-gray-500 text-sm px-4 text-white rounded'>
                            <span className='mr-1'>{langguage === 'id' ? t('lId') : t('lEn')}</span>
                            <Image alt="ico" src="/icon/icon-chevron.svg" width="16" height="16" />
                        </button>
                        {isDropdown && <div className="absolute top-[40px] flex flex-col bg-white text-left">
                            <Link onClick={() => {
                                dropdownHandler('en')
                            }} href="/login" locale="en" className='text-sm px-4 text-gray-500 border rounded py-2'>
                                <span className='mr-1'>{t('lEn')}</span>
                            </Link>
                            <Link onClick={() => {
                                dropdownHandler('id')
                            }} href="/login" locale="id" className='text-sm px-4 text-gray-500 border rounded py-2'>
                                <span className='mr-1'>{t('lId')}</span>
                            </Link>
                        </div>}

                    </div>
                </div>
                <div className="bg-city w-full h-[380px] md:bg-[url('/city.png')]">
                    <div className="container mx-auto px-4 pt-16 text-gray-500">
                        <Image src="/logo.png" alt="mileapp" width="200" height="200" className='mx-auto mb-4' />

                        {
                            (!isAuthenticated && isSubmited) && <div className="flex items-center">
                                <div className="bg-red-100 p-4 flex rounded mt-2 mb-5 mx-auto">
                                    <div className="flex space-x-2">
                                        <div className="rounded-full bg-red-500 flex w-[25px] text-white h-[25px] items-center justify-center">
                                            <Image alt="ico" src="/icon/icon-cross.svg" width="25" height="25" />

                                        </div>

                                        <p className='text-red-500 font-light text-sm'>
                                            {t("pwdIncorrect")}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        }


                        <div className=" md:w-5/12 bg-slate-100  mx-auto py-3 border-2 border-gray-200 rounded-sm px-10">
                            <h2 className="text-center text-3xl font-semibold text-sky-500">Login</h2>

                            <div className={isOrgValid ? "my-4 hidden" : "my-4"}>
                                <Input name="org" type="text" label={t('labelOrg')} placeholder={t('phOrg')} inputRef={org} onBlur={blurHandler} onChange={blurHandler} onKeyDown={handleKeyDown} />
                                <div className=" text-red-600 text-xs my-2 min-h-[1.3em]">{orgErrorMessage}</div>
                            </div>

                            <form onSubmit={onSubmitHandler}>
                                {isOrgValid &&
                                    <>
                                        <div className="my-4">
                                            <Input name="uname" type="text" label={t('labelUsername')} placeholder={t('phUsername')} inputRef={username} onBlur={blurHandler} onChange={blurHandler} />
                                            <div className=" text-red-600 text-xs my-2 min-h-[1.3em]">{usernameErrorMessage}</div>
                                        </div>
                                        <div className="my-4">
                                            <div className="relative">
                                                <Input name="pass" type={inputType} label={t('labelPwd')} placeholder={t('phPwd')} inputRef={password} onFocus={iconEyeHanlder} onBlur={blurHandler} onChange={blurHandler} />
                                                {isIconEyeOpen && <div className='absolute right-2 cursor-pointer top-[55%]' onClick={passwordReveal}>
                                                    {inputType === 'password' ?
                                                        <Image alt="ico" src="/icon/icon-eye.svg" width="16" height="16" />
                                                        :
                                                        <Image alt="ico" src="/icon/icon-eye-close.svg" width="16" height="16" />
                                                    }
                                                </div>}

                                            </div>
                                            <div className=" text-red-600 text-xs my-2 min-h-[1.3em]">{passwordErrorMessage}</div>
                                        </div>
                                    </>
                                }

                                <div className="flex flex-col items-center">
                                    {isLoggingIn ? <div className='px-10 py-2 rounded bg-blue-400 text-white text-sm flex cursor-not-allowed'>
                                        <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                            <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
                                            <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                                        </svg>
                                        Logging in...
                                    </div> :
                                        isOrgValid ?
                                            <button className='px-10 py-2 rounded bg-blue-400 text-white text-sm' type='submit'>
                                                {t('btnLogin')}
                                            </button> : <div className='px-10 py-2 rounded bg-blue-400 text-white text-sm cursor-pointer' onClick={orgHanlder} >
                                                {t('btnNext')}
                                            </div>}


                                </div>
                                <div className="text-center my-3">
                                    <p className='text-gray-400 font-light text-sm'>{t('regisYet')}
                                        <a href="" target="_blank" className='text-xs text-sky-500 ml-1'>{t('call')}</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer className='text-center text-sm font-light text-gray-500 mb-5 w-full'>
                © Copyright 2021 PT. Paket Informasi Digital. All Rights Reserved
            </footer>
        </div>
    )
}

export default Login
