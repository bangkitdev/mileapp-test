import { FocusEventHandler, KeyboardEventHandler, LegacyRef } from "react"

interface Props {
    label: string,
    placeholder: string,
    inputRef:  LegacyRef<HTMLInputElement>,
    type: string,
    name: string,
    onFocus?: FocusEventHandler<HTMLInputElement>
    onBlur?: FocusEventHandler<HTMLInputElement>
    onChange?: FocusEventHandler<HTMLInputElement>
    onKeyDown?: KeyboardEventHandler<HTMLInputElement>
}
const Input = (props: Props) => {
    return (
        <>
            <label htmlFor="first-name" className="block text-sm font-medium ">{props.label}</label>
            <input type={props.type}  className="mt-1 block w-full rounded-sm border-gray-300 p-3 focus:border-sky-300 placeholder:text-slate-300 placeholder:font-light  outline-sky-200" placeholder={props.placeholder} ref={props.inputRef} key={props.name} name={props.name} onFocus={props.onFocus} onBlur={props.onBlur} onChange={props.onChange} onKeyDown={props.onKeyDown}/>
        </>

    )
}

export default Input
