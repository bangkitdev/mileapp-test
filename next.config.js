/** @type {import('next').NextConfig} */
const nextTranslate = require("next-translate");

module.exports = nextTranslate({
    reactStrictMode: true,
    env: {
        NEXT_PUBLIC_MAPBOX_GL_ACCESS_TOKEN:
            process.env.NEXT_PUBLIC_MAPBOX_GL_ACCESS_TOKEN,
    },
});
