# 6 D ANSWER on PHP

```php
<?php

$data = [
    "1.", 
    "1.1.", 
    "1.2.", 
    "1.3.", 
    "1.4.", 
    "1.1.1.", 
    "1.1.2.", 
    "1.1.3.", 
    "1.2.1.", 
    "1.2.2.", 
    "1.3.1.", 
    "1.3.2.", 
    "1.3.3.", 
    "1.3.4.", 
    "1.4.1.", 
    "1.4.3."
];

$results = array_map(function($item) {
	$item = explode('.', $item);
	return array_filter($item);
}, $data);

$data = [];
foreach($results as $result) {
	if (isset($result[2])) {
		$data[$result[0]][$result[1]][$result[2]][]= implode('.', $result);
	}
}

var_dump(json_encode($data));
```
