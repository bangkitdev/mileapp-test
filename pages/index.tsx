import { NextPage } from 'next';
import Router from 'next/router';
import { signIn } from 'next-auth/react'
import { useEffect } from 'react';

const Home: NextPage = () => {
    useEffect(() => {
        // Router.push('/login/')
    }, [])
    return (
        <div className="min-w-full min-h-screen text-center flex items-center justify-center">
            <button className="px-10 py-2 rounded bg-blue-400 text-white text-sm" onClick={() => signIn()}>Login</button>
        </div>
    )
}

export default Home
