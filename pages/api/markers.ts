interface Marker {
    place: string;
    city: string;
    lat: number;
    long: number;
    iconType: 'blue' | 'orange';
}
export const markers: Marker[] = [
    {
        place: 'Monumen Nasional',
        city: 'Jakarta',
        lat: -6.174290534758097,
        long: 106.82696616985176,
        iconType:'blue'
    },
    {
        place: 'Gatot Soebroto Army Hospital',
        city: 'Jakarta',
        lat: -6.175879931592418,
        long: 106.8366821684742,
        iconType:'blue'
    },
    {
        place: 'Museum Sumpah Pemuda',
        city: 'Jakarta',
        lat: -6.183523170273638,
        long: 106.84300023383125,
        iconType:'orange'
    },
    {
        place: 'Cut Meutia Mosque',
        city: 'Jakarta',
        lat: -6.186858367055389,
        long: 106.83310379482677,
        iconType:'orange'
    },
    {
        place: 'Taman Prasasti Museum',
        city: 'Jakarta',
        lat: -6.171599669760248,
        long: 106.81906986132944,
        iconType:'blue'
    },
]
